package com.maks1mka.events

import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class ReadyEvent: ListenerAdapter() {

    override fun onReady(event: ReadyEvent) {
        println("[LOG] Ready - ${event.jda.selfUser.id} - ${event.jda.selfUser.name}")
        println("[LOG] Count guids - ${event.guildTotalCount}")
        return
    }
}