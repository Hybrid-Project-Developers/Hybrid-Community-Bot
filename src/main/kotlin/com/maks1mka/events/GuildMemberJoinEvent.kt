package com.maks1mka.events

import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class GuildMemberJoinEvent: ListenerAdapter() {

    override fun onGuildMemberJoin(event: GuildMemberJoinEvent) {
        event.guild.systemChannel?.sendMessage("Приветствую, ${event.member.asMention}")?.queue()
        return
    }
}