package com.maks1mka.commands.dev


import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.OWNERS
import com.maks1mka.utils.PREFIX
import net.dv8tion.jda.api.entities.ChannelType
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.ScriptException

class EvalCommand: BaseCommand() {
    override val name: String = "Eval"
    override val aliases: List<String> = listOf("eval", "e")
    override val desc: String = "Выполнение фрагмента кода на языке Java/Kotlin"
    override val category: String = "Разработчик"
    override val usage: String = "<code>"

    private var engine: ScriptEngine? = null
    init {
        engine = ScriptEngineManager().getEngineByName("nashorn")
        try {
            engine?.eval(
                "var imports = new JavaImporter(" +
                        "java.io," +
                        "java.lang," +
                        "java.util," +
                        "Packages.net.dv8tion.jda.api," +
                        "Packages.net.dv8tion.jda.api.entities," +
                        "Packages.net.dv8tion.jda.api.entities.impl," +
                        "Packages.net.dv8tion.jda.api.managers," +
                        "Packages.net.dv8tion.jda.api.managers.impl," +
                        "Packages.net.dv8tion.jda.api.utils);"
            )
        } catch (e: ScriptException) {
            e.printStackTrace()
        }
    }
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if (!OWNERS.contains(e.author.id)) {
            e.channel.sendMessage("Ты не мой создатель!").queue()
            return
        }
        try {

            with(engine!!) {
                put("event", e)
                put("message", e.message)
                put("channel", e.channel)
                put("args", args)
                put("api", e.jda)
            }


            if (e.isFromType(ChannelType.TEXT)) {
                engine?.put("guild", e.guild)
                engine?.put("member", e.member)
            }
             val out: Any? = engine?.eval(
                """(function() {
                        with (imports) {
                         return  ${e.message.contentRaw.substring((PREFIX + name).length)}
                        } 
                   })();
                """
             )

            e.channel.sendMessage(out?.toString() ?: "Выполнено без ошибок.").queue()
        }
        catch (e1: Exception)
        {
            e.channel.sendMessage(e1.message.toString()).queue()
        }
    }

}
