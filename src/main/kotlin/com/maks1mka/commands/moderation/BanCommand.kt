package com.maks1mka.commands.moderation

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.BaseEmbedBuilder
import com.maks1mka.utils.checkPerms
import com.maks1mka.utils.classException
import com.maks1mka.utils.fetchMember
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class BanCommand: BaseCommand() {
    override val name: String = "Ban"
    override val aliases: List<String> = listOf("ban")
    override val desc: String = "Бан участника сервера"
    override val category: String = "Модерация"
    override val usage: String = "<member> [reason]"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if (checkPerms(e, e.member!!.permissions)) return

        val member: Member? = fetchMember(e, args)
        if(classException(e, member!!.javaClass, "MemberImpl", true)) return

        if (member == e.member) {
            e.channel.sendMessage("Ты не можешь применять эту команду на себе").queue()
            return
        }

        var reason = "Причина не указана"
        val delDays = 7
        if(args.size > 1) reason = args.subList(1, args.size).joinToString(" ")

        val embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
           title = "Успешно!",
           desc =  "${member.asMention} забанен\n **Причина:** $reason\n**",
           color = e.author.retrieveProfile().complete().accentColor,
           textFooter = "Забанил ${e.author.name}",
           iconFooter = e.author.effectiveAvatarUrl
        )

        e.guild.ban(member, delDays, reason).queue().apply { e.channel.sendMessageEmbeds(embedBuilder.build()).queue() }

        return
    }

}