package com.maks1mka.commands.moderation

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.BaseEmbedBuilder
import com.maks1mka.utils.checkPerms
import com.maks1mka.utils.fetchUser
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class UnbanCommand: BaseCommand() {
    override val name: String = "Unban"
    override val aliases: List<String> = listOf("unban")
    override val desc: String = "Разбан участника сервера"
    override val category: String = "Модерация"
    override val usage: String = "<user>"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if (checkPerms(e, e.member!!.permissions)) return
        val user: User = fetchUser(e, args)

        if (user == e.author) {
            e.channel.sendMessage("Ты не можешь применять эту команду на себе").queue()
            return
        }

        val embedBuilder: EmbedBuilder = BaseEmbedBuilder().create(
            title = "Успешно!",
            desc = "${user.asMention} разбанен",
            color = e.author.retrieveProfile().complete().accentColor,
            textFooter = "Разбанил ${e.author.name}",
            iconFooter = e.author.effectiveAvatarUrl
        )
        e.guild.unban(user).queue().apply {
            e.channel.sendMessageEmbeds(embedBuilder.build()).queue()
        }
        return
    }
}