package com.maks1mka.commands.info

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.fetchUser
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class AvatarCommand: BaseCommand() {
    override val name: String = "Avatar"
    override val aliases: List<String> = listOf("avatar", "ava")
    override val desc: String = "Аватар заданного пользователя"
    override val category: String = "Информация"
    override val usage: String = "[user]"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        val user: User = fetchUser(e, args)

        val embed: MessageEmbed = EmbedBuilder()
            .setTitle("Аватарка ${user.asTag}")
            .setImage("${user.effectiveAvatarUrl}?size=4096")
            .setFooter("Запросил ${e.author.name}", e.author.effectiveAvatarUrl)
            .setColor(user.retrieveProfile().complete().accentColor)
            .build()

        e.channel.sendMessageEmbeds(embed).queue()
    }

}