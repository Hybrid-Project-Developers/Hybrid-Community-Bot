package com.maks1mka.commands.info

import com.maks1mka.commands.BaseCommand
import com.maks1mka.utils.*
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Emote
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.util.*

class UserProfileCommand: BaseCommand() {
    override val name: String = "UserProfile"
    override val aliases: List<String> = listOf("userprofile", "user", "profile", "pr", "info")
    override val desc: String = "Основная информация об участнике сервера"
    override val category: String = "Информация"
    override val usage: String = "[member]"
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        val member: Member? = fetchMember(e, args)
        if (classException(e, member!!.javaClass, "MemberImpl", true)) return

        var onlineStatus: String
        var userStatus = ""
        onlineStatus = member.onlineStatus.name
        val guildEmoji: Guild = e.jda.getGuildById(GUILD_FOR_EMOJI)!!
        when (onlineStatus) {
            "DO_NOT_DISTURB" -> {
                val dnd: Emote =  guildEmoji.getEmotesByName("dnd", false)[0]
                onlineStatus = dnd.asMention + "Не беспокоить"
            }
            "OFFLINE" -> {
                val off: Emote = guildEmoji.getEmotesByName("off", false)[0]
                onlineStatus = off.asMention + "Не в сети"
            }
            "ONLINE" -> {
                val on: Emote = guildEmoji.getEmotesByName("on", false)[0]
                onlineStatus = on.asMention + "В сети"
            }
            "IDLE" -> {
                val idle: Emote = guildEmoji.getEmotesByName("idle", false)[0]
                onlineStatus = idle.asMention + "Не активен"
            }
        }
        var userActivityString = "_Без активности_"
        if (member.activities.isNotEmpty()) {
            for(userActivity in member.activities) {
                var typeActivity = userActivity.type.name
                when (typeActivity) {
                    "WATCHING" -> typeActivity = "**Смотрит** "
                    "LISTENING" -> typeActivity = "**Слушает** "
                    "STREAMING" -> typeActivity = "**Стримит** "
                    "PLAYING" -> typeActivity = "**Играет в** "
                    "CUSTOM_STATUS" -> typeActivity =
                        (if (userActivity.emoji != null) userActivity.emoji!!.asMention else "") + " "
                }
                val nameActivity: String = typeActivity + userActivity.name
                var state: String? = userActivity.asRichPresence()?.state
                if(state.isNullOrEmpty()) state = "_Без статуса_"
                userActivityString = if (userActivity.timestamps != null) {
                    val startTime = userActivity.timestamps!!.start
                    val currentTime = Date().time
                    "$nameActivity \n**Состояние:** $state \n**Прошло:** ${getTimestamp(currentTime - startTime)}".trimIndent()
                } else {
                    if (userActivity.type.name == "CUSTOM_STATUS") {
                        userStatus = nameActivity
                        ""
                    } else nameActivity
                }
            }
        }

        var registeredTime = "${member.timeCreated.toZonedDateTime().toEpochSecond()}"
        registeredTime = "<t:$registeredTime:D> (<t:$registeredTime:R>)"
        var joinedTime = "${member.timeJoined.toZonedDateTime().toEpochSecond()}"
        joinedTime = "<t:$joinedTime:D> (<t:$joinedTime:R>)"

        val memberEmbed: EmbedBuilder = EmbedBuilder()
            .setTitle("Информация о " + member.user.asTag)
            .setColor(member.user.retrieveProfile().complete().accentColor)
            .setThumbnail(member.user.avatarUrl)
            .addField("Зарегистрировался", registeredTime, false)
            .addField("Присоединился", joinedTime, false)
            .addField("Сетевой статус", onlineStatus, true)
            .setFooter("Запросил ${e.author.name}", e.author.effectiveAvatarUrl)
        if(userActivityString.isNotEmpty()) memberEmbed.addField("Активность", userActivityString, true)
        if(userStatus.isNotEmpty()) memberEmbed.addField("Пользовательский статус", userStatus, false)

        e.channel.sendMessageEmbeds(memberEmbed.build()).queue()


    }


}