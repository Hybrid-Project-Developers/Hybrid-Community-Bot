package com.maks1mka.commands.music

import com.maks1mka.commands.BaseCommand
import com.maks1mka.commands.music.lavaplayer.GuildMusicManager
import com.maks1mka.utils.isBotNotInVoice
import com.maks1mka.utils.isUserNotInVoice
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class LeaveCommand: BaseCommand() {
    override val name: String = "Leave"
    override val aliases: List<String> = listOf("leave", "disconnect", "stop")
    override val desc: String = "Отключение от голосового канала"
    override val category: String = "Музыка"
    override val usage: String = ""
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if(isUserNotInVoice(e) || isBotNotInVoice(e)) return
        val manager: GuildMusicManager = PlayCommand.getMusicManager(e.guild)
        manager.player.destroy()
        e.guild.audioManager.closeAudioConnection().apply {
            e.channel.sendMessage("Я отключилась от **${e.member!!.voiceState!!.channel!!.name}**").queue()
        }
        return

    }
}