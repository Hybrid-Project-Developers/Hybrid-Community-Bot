package com.maks1mka.commands.music

import com.maks1mka.commands.BaseCommand
import com.maks1mka.commands.music.lavaplayer.GuildMusicManager
import com.maks1mka.utils.getTimestamp
import com.maks1mka.utils.isBotNotInVoice
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.util.*

class QueueCommand: BaseCommand() {
    override val name: String = "Queue"
    override val aliases: List<String> = listOf("queue", "list", "q", "l")
    override val desc: String = "Посмотр текущей очереди треков"
    override val category: String = "Музыка"
    override val usage: String = ""
    override fun onExecute(e: MessageReceivedEvent, name: String, args: List<String>) {

        if(isBotNotInVoice(e, true)) return

        val manager: GuildMusicManager  = PlayCommand.getMusicManager(e.guild)

        val queue: Queue<AudioTrack> = manager.scheduler.getQueue()

        synchronized(queue) {
            if (queue.isEmpty()) {
                e.channel.sendMessage("Очередь пуста, дурашка!").queue()
                return
            }

            var trackCount = 0
            var queueLength: Long = 0

            val sb: StringBuilder = StringBuilder()
            sb.append("Первые 30 треков очереди").append("\n")
            for (track in queue) {
                queueLength += track.duration
                if (trackCount < 30) {
                    sb.append("**").append(trackCount + 1).append(") [").append(track.info.title).append("](")
                        .append(track.info.uri).append(")** (").append(getTimestamp(track.duration)).append(")")
                        .append("\n")
                    trackCount++
                } else break
            }

            sb.append("\n").append("Треков в очереди: ").append(queue.size).append("\n")
            sb.append("Длительность очереди: ").append(getTimestamp(queueLength))
            val currentAuthor = e.member!!
            val listEmbed = EmbedBuilder()
                .setTitle("Очередь треков")
                .setDescription(sb.toString())
                .setColor(currentAuthor.user.retrieveProfile().complete().accentColor)
                .setFooter(
                    "Запросил " + if (currentAuthor.nickname == null) currentAuthor.user.name else currentAuthor.nickname,
                    currentAuthor.user.avatarUrl
                )
                .build()
            e.channel.sendMessageEmbeds(listEmbed).queue()
        }
    }
}