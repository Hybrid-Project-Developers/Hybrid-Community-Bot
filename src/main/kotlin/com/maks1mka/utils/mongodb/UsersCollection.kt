package com.maks1mka.utils.mongodb

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import org.bson.Document


class UsersCollection(client: MongoClient, db: MongoDatabase) {

    private var users: MongoCollection<Document>?
    private val db: MongoDatabase
    private val client: MongoClient

    init {
        this.client = client
        this.db = db
        this.users = this.db.getCollection("users")
    }

    fun getUserById(guildId: String, userId: String): Document? {
        if (users?.find(Document("guild_id", guildId).append("_id", userId))?.first() == null) {
            this.addUser(guildId, userId)
        }
        return users!!.find(Document("_id", userId)).first()
    }

    private fun checkUserNotExists(guildId: String, userId: String): Boolean {
        return users!!.find(Document("guild_id", guildId).append("_id", userId)).first().isNullOrEmpty()
    }

    fun addUser(guildId: String, userId: String) {
        val userObj: Document = Document("guild_id", guildId,)
            .append("_id", userId)
            .append("warns", 0)
        users!!.insertOne(this.client.startSession(), userObj).apply {
            println("[LOG] user added")
        }
        return
    }

    fun updateUser(guildId: String, userId: String, op: String = "\$set", name: String, value: Any): Document? {

        val findParams: Document = Document("guild_id", guildId,)
            .append("_id", userId)
        val updateParams = Document(op, Document(name, value))

        if (checkUserNotExists(guildId, userId)) addUser(guildId, userId)

        users!!.updateOne(this.client.startSession(), findParams, updateParams)

        return getUserById(guildId, userId)

    }

    fun deleteUser(guildId: String, userId: String) {
        val userObj: Document = Document("guild_id", guildId,)
            .append("_id", userId)
        users!!.deleteOne(this.client.startSession(), userObj)
    }

}