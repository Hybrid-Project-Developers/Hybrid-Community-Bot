package com.maks1mka.handlers

import com.maks1mka.commands.dev.*
import com.maks1mka.commands.info.*
import com.maks1mka.commands.`fun`.*
import com.maks1mka.commands.moderation.*
import com.maks1mka.commands.music.*
import net.dv8tion.jda.api.JDABuilder

class CommandHandler(builder: JDABuilder) {
    private val builder: JDABuilder

    init {
        this.builder = builder
    }

    fun initCommands(): JDABuilder {

        val help = HelpCommand()

        help.registerCommands(
            listOf(
                help,
                //DEV
                PingCommand(),
                EvalCommand(),

                //INFO
                AvatarCommand(),
                BannerCommand(),
                UserProfileCommand(),

                //MODERATION
                BanCommand(),
                UnbanCommand(),
                KickCommand(),
                MuteCommand(),
                UnmuteCommand(),
                WarnCommand(),
                WarnsCommand(),
                PurgeCommand(),

                //FUN
                ActionCommand(),
                AnimeSceneSearchCommand(),
                QuoteCommand(),

                //MUSIC
                JoinCommand(),
                LeaveCommand(),
                PlayCommand(),
                ClearQueueCommand(),
                SkipCommand(),
                PauseCommand(),
                RepeatCommand(),
                SearchCommand(),
                QueueCommand(),
                NowPlayingCommand(),
                ShuffleCommand(),


            ),
            this.builder
        )

        println("[LOG] Commands loaded")
        return this.builder
    }
}
