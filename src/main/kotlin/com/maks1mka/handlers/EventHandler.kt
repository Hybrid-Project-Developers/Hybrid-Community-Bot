package com.maks1mka.handlers

import com.maks1mka.events.*
import net.dv8tion.jda.api.JDABuilder

class EventHandler(builder: JDABuilder) {

    private val builder: JDABuilder

    init {
        this.builder = builder
    }

    fun initEvents(): JDABuilder {
        with(this.builder) {
            addEventListeners(
                ReadyEvent(),
                GuildMemberJoinEvent(),
            )
        }.apply {
            println("[LOG] Events loaded")
        }
        return this.builder
    }
}